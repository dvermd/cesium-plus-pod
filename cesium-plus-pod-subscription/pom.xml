<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.duniter.cesium</groupId>
    <artifactId>cesium-plus-pod</artifactId>
    <version>1.6.4-SNAPSHOT</version>
  </parent>

  <artifactId>cesium-plus-pod-subscription</artifactId>
  <packaging>jar</packaging>
  <name>Cesium+ pod :: Subscription plugin</name>
  <description>An ElasticSearch plugin that can send email notifications to end users</description>

  <properties>

    <!-- i18n configuration -->
    <i18n.bundleOutputName>${project.artifactId}-i18n</i18n.bundleOutputName>
    <i18n.generateCsvFile>true</i18n.generateCsvFile>
    <i18n.bundleCsvFile>
      ${maven.gen.dir}/resources/META-INF/${i18n.bundleOutputName}.csv
    </i18n.bundleCsvFile>
    <config.i18nBundleName>${i18n.bundleOutputName}</config.i18nBundleName>
  </properties>

  <dependencies>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>${project.parent.artifactId}-core</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>${project.parent.artifactId}-user</artifactId>
      <version>${project.version}</version>
    </dependency>

    <!-- Elastic Search -->
    <dependency>
      <groupId>org.elasticsearch</groupId>
      <artifactId>elasticsearch</artifactId>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.antlr</groupId>
      <artifactId>ST4</artifactId>
      <version>${stringtemplate.version}</version>
      <scope>provided</scope>
    </dependency>

    <!-- Unit test -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-log4j12</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <optional>true</optional>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>net.java.dev.jna</groupId>
      <artifactId>jna</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>net.java.dev.jna</groupId>
      <artifactId>jna-platform</artifactId>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <groupId>net.java.dev.jna</groupId>
          <artifactId>jna</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.elasticsearch.plugin</groupId>
      <artifactId>mapper-attachments</artifactId>
      <version>${elasticsearch.version}</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <groupId>commons-logging</groupId>
          <artifactId>commons-logging</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <!-- Websocket (need for test) -->
    <dependency>
      <groupId>javax.websocket</groupId>
      <artifactId>javax.websocket-api</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.glassfish.tyrus</groupId>
      <artifactId>tyrus-server</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.glassfish.tyrus</groupId>
      <artifactId>tyrus-container-grizzly-server</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <resources>
      <resource>
        <directory>src/main/filtered-resources</directory>
        <filtering>true</filtering>
        <includes>
          <include>*.config</include>
          <include>**/*.properties</include>
        </includes>
      </resource>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>false</filtering>
      </resource>
    </resources>

    <plugins>
      <plugin>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>i18n-maven-plugin</artifactId>

        <executions>
          <execution>
            <id>scan-sources</id>
            <configuration>
              <entries>
                <entry>
                  <specificGoal>parserValidation</specificGoal>
                  <basedir>${maven.src.dir}/main/java/</basedir>
                  <includes>
                    <param>**/**-validation.xml</param>
                  </includes>
                </entry>
              </entries>
            </configuration>
            <goals>
              <goal>parserJava</goal>
              <goal>parserValidation</goal>
              <goal>gen</goal>
            </goals>
          </execution>
          <execution>
            <id>make-bundle</id>
            <goals>
              <goal>bundle</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <executions>
          <execution>
            <id>assembly-plugin</id>
            <phase>package</phase>
            <goals>
              <goal>single</goal>
            </goals>
            <configuration>
              <attach>true</attach>
              <appendAssemblyId>false</appendAssemblyId>
              <finalName>${project.artifactId}-${project.version}</finalName>
              <descriptors>
                <descriptor>
                  ${basedir}/src/main/assembly/plugin.xml
                </descriptor>
              </descriptors>
              <skipAssembly>${assembly.skip}</skipAssembly>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
